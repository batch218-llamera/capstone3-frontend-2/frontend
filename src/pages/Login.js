import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(true);
    function authenticate(e) {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Let's Start Exploring!"
                })
            } 
            else {
                    Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })

            };
        });
        setEmail('');
        setPassword('');
    };

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };

useEffect(() => {
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [email, password]);


return (
    (user.id !== null) ?
    <Navigate to ="/" />
    :   
    <Container>
            <Row>
                <Col md={6} className="mt-5 login__form--container">
                    <Form style={{ width: "100%" }} onSubmit={(e) => authenticate(e)}>
                        <h1 className="mb-4">Login to your account</h1>
                        <Form.Group className="mb-3" controlId="userEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control
                                style={{ width: "60%" }}
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                required
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-4" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                style={{ width: "60%" }}
                                type="password"
                                placeholder="Enter Password"
                                value={password}
                                required
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            { isActive ? 
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Submit
                                </Button>
                                : 
                                <Button variant="primary" type="submit" id="submitBtn" disabled>
                                    Submit
                                </Button>
                            }  
                        </Form.Group>
                        <p className="pt-3">
                            Don't have an account? <Link to="/register">Create account</Link>{" "}
                        </p>
                    </Form>
                </Col>
                <Col md={6} className="mt-5 login__image--container"></Col>
            </Row>
        </Container>



    // <Form onSubmit={(e) => authenticate(e)}>
    //     <Form.Group controlId="userEmail">
    //         <Form.Label>Email address</Form.Label>
    //         <Form.Control 
    //             type="email" 
    //             placeholder="Enter email"
    //             value={email}
    //             onChange={(e) => setEmail(e.target.value)} 
    //             required
    //         />
    //     </Form.Group>

    //     <Form.Group controlId="password">
    //         <Form.Label>Password</Form.Label>
    //         <Form.Control 
    //             type="password" 
    //             placeholder="Password" 
    //             value={password}
    //             onChange={(e) => setPassword(e.target.value)}
    //             required
    //         />
    //     </Form.Group>

    //      { isActive ? 
    //     <Button variant="primary" type="submit" id="submitBtn">
    //         Submit
    //     </Button>
    //     : 
    //     <Button variant="danger" type="submit" id="submitBtn" disabled>
    //         Submit
    //     </Button>
    // }
    // </Form>
    )
}