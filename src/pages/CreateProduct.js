import { useState, useEffect,useContext } from 'react'; 

import {Navigate} from 'react-router-dom'; 
import {useNavigate} from 'react-router-dom'; 

import Swal from 'sweetalert2'; 

import UserContext from '../UserContext'; 

import { Form, Button } from 'react-bootstrap';

export default function Add() {
    const {user} = useContext(UserContext); 
    const navigate = useNavigate(); 

    const [name, setName] = useState(""); 
    const [description, setDescription] = useState(""); 
    const [price, setPrice] = useState("");
    const [isActive, setIsActive] = useState(false);

    function addProduct(e) {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/products/checkProductExist`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data === true) {
                Swal.fire({
                    title: "Duplicate Product",
                    icon: "error",
                    text: "Kindly provide another Product."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price, 
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {
            
                    setName("");
                    setDescription("")
                    setPrice("");
             

                    Swal.fire({
                            title: "Added Product",
                            icon: "success",
                            text: "Add another product"
                        })

                        navigate("/addproduct");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }
                })
            }
        })    
    }

    
    useEffect(() => {
            if((name !== '' && description !== '' && price !== '')){
                setIsActive(true);
            } else {
                setIsActive(false);
            }
        }, [name, description, price])

    return (
        <Form onSubmit={(e) => addProduct(e)}>
            <Form.Group className="mb-3" controlId="name">
            <Form.Label>Product Name</Form.Label>
            <Form.Control 
                type="text"
                value={name}
                onChange={(e) => {setName(e.target.value)}}
                placeholder="Enter Product Name" 
                required
                />
          </Form.Group>

          <Form.Group className="mb-3" controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control 
                type="text"
                value={description}
                onChange={(e) => {setDescription(e.target.value)}}
                placeholder="Enter Description" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="price">
            <Form.Label>Price</Form.Label>
            <Form.Control 
                type="number"
                value={price}
                onChange={(e) => {setPrice(e.target.value)}}
                placeholder="Enter Price" />
            <Form.Text className="text-muted">
              
            </Form.Text>
          </Form.Group>

          { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                     Add
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                      Add
                    </Button>
          }
         
        </Form> 
    )

}

