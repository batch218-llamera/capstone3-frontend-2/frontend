import { useState, useEffect,useContext } from 'react'; 
import { Container, Card,Row, Col } from 'react-bootstrap';
import {Navigate} from 'react-router-dom'; 
import {useNavigate, useParams, Link} from 'react-router-dom'; 
import ProductCard from '../components/ShowAllProducts';

import Swal from 'sweetalert2'; 

import UserContext from '../UserContext'; 

import { Form, Button } from 'react-bootstrap';

export default function Add() {
    const { productId } = useParams();
    const {user} = useContext(UserContext); 

    const navigate = useNavigate(); 

    const [name, setName] = useState(""); 
    const [description, setDescription] = useState(""); 
    const [price, setPrice] = useState("");
    const [isActive, setIsActive] = useState(true);
    const [products, setProducts] = useState([]);
 
    function searchProduct(e, productId) {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/products/checkProductExist`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data !== true) {
                Swal.fire({
                    title: "No product found",
                    icon: "error",
                    text: "Kindly provide another Product."
                })
            } else {
                Swal.fire({
                    title: "Product found",
                    icon: "success",
                    text: "Search another Product."
        })      
            }
        })
    }

    useEffect(() => {
            if((name !== "")){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [name])
    return (
        <Form onSubmit={(e) => searchProduct(e)}>
            <Form.Group className="mt-3 mb-3" controlId="name">
            <Form.Label><h1>Product Name</h1></Form.Label>
            <Form.Control 
                type="text"
                value={name}
                onChange={(e) => {setName(e.target.value)}}
                placeholder="Enter Product Name" 
                required
                />
          </Form.Group>
          { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                     Find Product
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                      Find Product
                    </Button>
          }
        </Form> 
    )
}