import {useState, useContext} from 'react';
import {Link, NavLink, useParams} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from '../UserContext';


export default function AppNavbar() {
  const {user } = useContext(UserContext);
  const { userId } = useParams();
  return (
    <Navbar bg="dark" expand="lg" sticky="top">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/" className="text-white">Home</Nav.Link>
              { 
                (user.isAdmin === true && user.id !== null) &&
                <>
                    <Nav.Link as={NavLink} to="/addproduct" className="text-white">Add Product</Nav.Link>
                    <Nav.Link as={NavLink} to="/products" className="text-white">Admin Dashboard</Nav.Link>
                    <Nav.Link as={NavLink} to="/logout" className="text-white">Logout</Nav.Link>
                </>
              }
              { 
                (user.isAdmin === false && user.id !== null) &&
                <>  
                    <Nav.Link as={NavLink} to="/search" className="text-white">Products</Nav.Link>
                    <Nav.Link as={NavLink} to="/logout" className="text-white">Logout</Nav.Link>
                      <Nav.Link as={NavLink} to="/user/:userId" className="text-white">Orders</Nav.Link>       
                </>
              }
              { 
                (user.id === null ) &&
                <>
                  <Nav.Link as={NavLink} to="/login" className="text-white">Login</Nav.Link> 
                  <Nav.Link as={NavLink} to="/register" className="text-white">Register</Nav.Link>
                  <Nav.Link as={NavLink} to="/search" className="text-white">Products</Nav.Link>   
                </>
              }
          </Nav>
        </Navbar.Collapse>
    </Navbar>
  );
}